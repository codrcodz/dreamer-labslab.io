FROM ruby:2.6.3-stretch

VOLUME /src
COPY . /src
WORKDIR /src
RUN gem install bundler:2.0.1 && bundle install
RUN bundle exec jekyll build -d public
EXPOSE 4000
ENTRYPOINT ["bundle", "exec", "jekyll", "serve", "--livereload", "-H", "0.0.0.0"]
